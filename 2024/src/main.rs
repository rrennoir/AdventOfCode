use std::env;

pub mod common;
pub mod day01;
pub mod day02;
pub mod day03;
pub mod day04;

fn main() {
    let args = env::args().into_iter().collect::<Vec<String>>();

    if args.len() < 2 {
        println!("AdventOfCode <day>");
        return;
    }

    match args[1].as_str() {
        "01" | "1" => day01::day_01(),
        "02" | "2" => day02::day_02(),
        "03" | "3" => day03::day_03(),
        "04" | "4" => day04::day_04(),
        _ => unimplemented!(),
    };
}
