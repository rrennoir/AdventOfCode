use std::fs;

pub fn day_04() {
    let input_1 = fs::read_to_string("inputs/day04/input-1.txt").expect("failed to read input");
    let result_part1 = part_1(&input_1);
    println!("Result of part 1: {result_part1}");
    // assert_eq!(result_part1, 2571);

    let input_2 = fs::read_to_string("inputs/day04/input-1.txt").expect("failed to read input");
    let result_part2 = part_2(&input_2);
    println!("Result of part 2: {result_part2}");
    //assert_eq!(result_part2, 1992);
}

fn xmas_check(x: char, m: char, a: char, s: char) -> bool {
    x == 'X' && m == 'M' && a == 'A' && s == 'S'
}

fn backwards_check(map: &Vec<Vec<char>>, x: usize, y: usize, visual: &mut Vec<Vec<char>>) -> i32 {
    if xmas_check(map[y][x], map[y][x - 1], map[y][x - 2], map[y][x - 3]) {
        visual[y][x - 3] = map[y][x - 3];
        visual[y][x - 2] = map[y][x - 2];
        visual[y][x - 1] = map[y][x - 1];
        visual[y][x] = map[y][x];
        return 1;
    }
    0
}

fn forwards_check(map: &Vec<Vec<char>>, x: usize, y: usize, visual: &mut Vec<Vec<char>>) -> i32 {
    if xmas_check(map[y][x], map[y][x + 1], map[y][x + 2], map[y][x + 3]) {
        visual[y][x + 3] = map[y][x + 3];
        visual[y][x + 2] = map[y][x + 2];
        visual[y][x + 1] = map[y][x + 1];
        visual[y][x] = map[y][x];
        return 1;
    }
    0
}

fn backwards_up_check(
    map: &Vec<Vec<char>>,
    x: usize,
    y: usize,
    visual: &mut Vec<Vec<char>>,
) -> i32 {
    if xmas_check(
        map[y][x],
        map[y - 1][x - 1],
        map[y - 2][x - 2],
        map[y - 3][x - 3],
    ) {
        visual[y - 3][x - 3] = map[y - 3][x - 3];
        visual[y - 2][x - 2] = map[y - 2][x - 2];
        visual[y - 1][x - 1] = map[y - 1][x - 1];
        visual[y][x] = map[y][x];
        return 1;
    }
    0
}

fn backwards_down_check(
    map: &Vec<Vec<char>>,
    x: usize,
    y: usize,
    visual: &mut Vec<Vec<char>>,
) -> i32 {
    if xmas_check(
        map[y][x],
        map[y + 1][x - 1],
        map[y + 2][x - 2],
        map[y + 3][x - 3],
    ) {
        visual[y + 3][x - 3] = map[y + 3][x - 3];
        visual[y + 2][x - 2] = map[y + 2][x - 2];
        visual[y + 1][x - 1] = map[y + 1][x - 1];
        visual[y][x] = map[y][x];
        return 1;
    }
    0
}

fn forwards_up_check(map: &Vec<Vec<char>>, x: usize, y: usize, visual: &mut Vec<Vec<char>>) -> i32 {
    if xmas_check(
        map[y][x],
        map[y - 1][x + 1],
        map[y - 2][x + 2],
        map[y - 3][x + 3],
    ) {
        visual[y - 3][x + 3] = map[y - 3][x + 3];
        visual[y - 2][x + 2] = map[y - 2][x + 2];
        visual[y - 1][x + 1] = map[y - 1][x + 1];
        visual[y][x] = map[y][x];
        return 1;
    }
    0
}

fn forwards_down_check(
    map: &Vec<Vec<char>>,
    x: usize,
    y: usize,
    visual: &mut Vec<Vec<char>>,
) -> i32 {
    if xmas_check(
        map[y][x],
        map[y + 1][x + 1],
        map[y + 2][x + 2],
        map[y + 3][x + 3],
    ) {
        visual[y + 3][x + 3] = map[y + 3][x + 3];
        visual[y + 2][x + 2] = map[y + 2][x + 2];
        visual[y + 1][x + 1] = map[y + 1][x + 1];
        visual[y][x] = map[y][x];
        return 1;
    }
    0
}

fn up_check(map: &Vec<Vec<char>>, x: usize, y: usize, visual: &mut Vec<Vec<char>>) -> i32 {
    if xmas_check(map[y][x], map[y - 1][x], map[y - 2][x], map[y - 3][x]) {
        visual[y - 3][x] = map[y - 3][x];
        visual[y - 2][x] = map[y - 2][x];
        visual[y - 1][x] = map[y - 1][x];
        visual[y][x] = map[y][x];
        return 1;
    }
    0
}

fn down_check(map: &Vec<Vec<char>>, x: usize, y: usize, visual: &mut Vec<Vec<char>>) -> i32 {
    if xmas_check(map[y][x], map[y + 1][x], map[y + 2][x], map[y + 3][x]) {
        visual[y + 3][x] = map[y + 3][x];
        visual[y + 2][x] = map[y + 2][x];
        visual[y + 1][x] = map[y + 1][x];
        visual[y][x] = map[y][x];
        return 1;
    }
    0
}

fn part_1(input: &str) -> i32 {
    let map: Vec<Vec<char>> = input.lines().map(|line| line.chars().collect()).collect();

    let mut visual: Vec<Vec<char>> = Vec::with_capacity(map.len());

    for y in 0..map.len() {
        visual.push(Vec::with_capacity(map[y].len()));
        for _ in 0..map[y].len() {
            visual[y].push('.');
        }
    }

    let mut counter = 0;

    let mut y = 0;
    let max_y = map.len();
    while y < max_y {
        let mut x = 0;
        let max_x = map[y].len();
        while x < max_x {
            if x >= 3 {
                counter += backwards_check(&map, x, y, &mut visual);
                if y >= 3 {
                    counter += backwards_up_check(&map, x, y, &mut visual);
                }
                if y < max_y - 3 {
                    counter += backwards_down_check(&map, x, y, &mut visual);
                }
            }

            if x < max_x - 3 {
                counter += forwards_check(&map, x, y, &mut visual);
                if y >= 3 {
                    counter += forwards_up_check(&map, x, y, &mut visual);
                }
                if y < max_y - 3 {
                    counter += forwards_down_check(&map, x, y, &mut visual);
                }
            }

            if y < max_y - 3 {
                counter += down_check(&map, x, y, &mut visual);
            }

            if y >= 3 {
                counter += up_check(&map, x, y, &mut visual);
            }

            x += 1;
        }
        y += 1;
    }

    for row in visual {
        for char in row {
            print!("{}", char);
        }
        println!("");
    }

    counter
}

fn xmas_check_2(map: &Vec<Vec<char>>, x: usize, y: usize) -> bool {
    ((map[y - 1][x - 1] == 'M' && map[y + 1][x + 1] == 'S')
        || (map[y - 1][x - 1] == 'S' && map[y + 1][x + 1] == 'M'))
        && ((map[y + 1][x - 1] == 'M' && map[y - 1][x + 1] == 'S')
            || (map[y + 1][x - 1] == 'S' && map[y - 1][x + 1] == 'M'))
}

fn part_2(input: &str) -> i32 {
    let map: Vec<Vec<char>> = input.lines().map(|line| line.chars().collect()).collect();
    let mut counter = 0;

    let mut y = 0;
    let max_y = map.len();
    while y < max_y {
        let mut x = 0;
        let max_x = map[y].len();
        while x < max_x {
            if x == 0 || x == max_x - 1 || y == 0 || y == max_y - 1 || map[y][x] != 'A' {
                x += 1;
                continue;
            }

            if xmas_check_2(&map, x, y) {
                counter += 1;
            }

            x += 1;
        }
        y += 1;
    }
    counter
}
