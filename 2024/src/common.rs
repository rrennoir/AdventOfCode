use std::fs;

pub fn load_input(day: usize, part: Option<usize>) {
    let path = format!("inputs/day{}/input{}.txt", day, part.unwrap());

    let input = fs::read_to_string(path).expect("Couldn't read file");

}
