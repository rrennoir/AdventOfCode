use std::fs;

pub fn day_02() {
    let input = fs::read_to_string("inputs/day02/input-1.txt").expect("Failed to read input");
    let result_part1 = part_1(&input);
    println!("Result of part 1: {result_part1}");
    assert_eq!(result_part1, 379);

    let result_part2 = part_2(&input);
    println!("Result of part 2: {result_part2}");
    assert_eq!(result_part2, 430);
}

fn parse_reports(input: &str) -> Vec<Vec<i32>> {
    input
        .lines()
        .map(|line| {
            line.split_whitespace()
                .map(|level| level.parse().unwrap())
                .collect()
        })
        .collect()
}

fn check_report(report: &Vec<i32>) -> bool {
    let mut trend: Option<i32> = None;
    for pair in report.windows(2) {
        let diff = pair[1] - pair[0];
        if diff.abs() > 3 || diff.signum() == 0 {
            return false;
        }
        if trend.is_none() {
            trend = Some(diff.signum());
            continue;
        }

        if trend.is_some_and(|x| x != diff.signum()) {
            return false;
        }
    }
    return true;
}

fn part_1(input: &str) -> i32 {
    let reports = parse_reports(input);
    let mut good_reports = 0;
    for report in reports {
        if check_report(&report) {
            good_reports += 1;
        }
    }
    good_reports
}

fn part_2(input: &str) -> i32 {
    let reports = parse_reports(input);

    let mut good_reports = 0;
    let mut i = 0;
    let mut cur: i32 = -1;
    while i < reports.len() {
        let mut report = reports[i].clone();

        if cur != -1 {
            report.remove(cur.try_into().unwrap());
        }

        if check_report(&report) {
            good_reports += 1;
        } else if cur < report.len() as i32 {
            cur += 1;
            continue;
        }

        i += 1;
        cur = -1;
    }
    good_reports
}

#[test]
fn test_part1() {
    let result = part_1(include_str!("../inputs/day02/test-1.txt"));
    assert_eq!(result, 2)
}

#[test]
fn test_part2() {
    let result = part_2(include_str!("../inputs/day02/test-1.txt"));
    assert_eq!(result, 4)
}
