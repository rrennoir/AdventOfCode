use std::fs;

pub fn day_03() {
    let input = fs::read_to_string("inputs/day03/input-1.txt").expect("Failed to read input");
    let result_part1 = part_1(&input);
    println!("Result of part 1: {result_part1}");
    // assert_eq!(result_part1, 162813399);

    let result_part2 = part_2(&input);
    println!("Result of part 2: {result_part2}");
    //assert_eq!(result_part2, 21024792);
}

fn part_1(input: &str) -> i32 {
    let mut result = 0;
    let ops = input.split("mul(").collect::<Vec<&str>>();
    for op in ops {
        if !op.contains(")") {
            continue;
        }
        let args = op
            .split(")")
            .collect::<Vec<&str>>()
            .first()
            .unwrap()
            .to_owned();

        if !args.contains(&",") {
            continue;
        }

        let (a, b) = args.split_once(',').unwrap();

        let n1: i32 = match a.parse() {
            Ok(number) => number,
            Err(_) => continue,
        };

        let n2: i32 = match b.parse() {
            Ok(number) => number,
            Err(_) => continue,
        };
        result += n1 * n2;
    }
    result
}

fn part_2(input: &str) -> i32 {
    let mut result = 0;

    let splits = input.split("do()").collect::<Vec<&str>>();

    for split in splits {

        dbg!(&split);

        let good_ops = match split.split_once("don't()") {
            Some(ops) => ops.0,
            None => split,
        };

        let ops = dbg!(good_ops.split("mul(").collect::<Vec<&str>>());
        for op in ops {
            if !op.contains(")") {
                continue;
            }
            let args = op
                .split(")")
                .collect::<Vec<&str>>()
                .first()
                .unwrap()
                .to_owned();

            if !args.contains(&",") {
                continue;
            }

            let (a, b) = args.split_once(',').unwrap();

            let n1: i32 = match a.parse() {
                Ok(number) => number,
                Err(_) => continue,
            };

            let n2: i32 = match b.parse() {
                Ok(number) => number,
                Err(_) => continue,
            };
            result += n1 * n2;
        }
    }

    result
}

#[test]
fn test_part1() {
    let result = part_1(include_str!("../inputs/day03/test-1.txt"));
    assert_eq!(result, 161)
}

#[test]
fn test_part2() {
    let result = part_2(include_str!("../inputs/day03/test-1.txt"));
    assert_eq!(result, 48)
}
