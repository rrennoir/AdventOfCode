use std::{collections::HashMap, fs};

pub fn day_01() {
    let input = fs::read_to_string("inputs/day01/input-1.txt").expect("Failed to read input");
    let result_part1 = part_1(&input);
    println!("Result of part 1: {result_part1}");
    assert_eq!(result_part1, 1879048);

    let result_part2 = part_2(&input);
    println!("Result of part 2: {result_part2}");
    assert_eq!(result_part2, 21024792);
}

fn part_1(input: &str) -> u32 {
    let mut left_numbers: Vec<i32> = Vec::new();
    let mut right_numbers: Vec<i32> = Vec::new();

    let lines = input.lines().collect::<Vec<&str>>();

    for line in lines {
        let numbers = line.split_whitespace().collect::<Vec<&str>>();
        left_numbers.push(numbers.first().unwrap().parse().unwrap());
        right_numbers.push(numbers.last().unwrap().parse().unwrap());
    }

    left_numbers.sort();
    right_numbers.sort();

    left_numbers
        .iter()
        .zip(right_numbers.iter())
        .map(|(&x, &y)| x.abs_diff(y))
        .sum()
}

fn part_2(input: &str) -> i32 {
    let mut left_numbers: Vec<i32> = Vec::new();
    let mut right_numbers: HashMap<i32, i32> = HashMap::new();

    let lines = input.lines().collect::<Vec<&str>>();

    for line in lines {
        let numbers = line.split_whitespace().collect::<Vec<&str>>();
        left_numbers.push(numbers.first().unwrap().parse::<i32>().unwrap());
        right_numbers
            .entry(numbers.last().unwrap().parse::<i32>().unwrap())
            .and_modify(|count| *count += 1)
            .or_insert(1);
    }

    left_numbers
        .iter()
        .map(|x| x * right_numbers.get(&x).or(Some(&0)).unwrap())
        .sum()
}

#[test]
fn test_part1() {
    let result = part_1(include_str!("../inputs/day01/test-1.txt"));
    assert_eq!(result, 11)
}

#[test]
fn test_part2() {
    let result = part_2(include_str!("../inputs/day01/test-1.txt"));
    assert_eq!(result, 31)
}
