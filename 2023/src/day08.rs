use std::{collections::HashMap, fs, str::Lines};

#[derive(Debug)]
enum Direction {
    Left,
    Right,
}

impl Direction {
    fn from_str(char: &char) -> Direction {
        match char {
            'L' => Direction::Left,
            'R' => Direction::Right,
            _ => unreachable!(),
        }
    }
}

fn parse_instructions(line: &str) -> Vec<Direction> {
    line.chars()
        .map(|c| Direction::from_str(&c))
        .collect::<Vec<Direction>>()
}

fn parse_choices(lines: Lines) -> HashMap<&str, (&str, &str)> {
    let mut map: HashMap<&str, (&str, &str)> = HashMap::new();
    for line in lines {
        let (origin, rest) = line.split_once(" = (").unwrap();
        let (left, rest) = rest.split_once(", ").unwrap();
        let (rigth, _) = rest.split_once(")").unwrap();
        map.insert(origin, (left, rigth));
    }
    map
}

fn find_path(
    map: &HashMap<&str, (&str, &str)>,
    instructions: &Vec<Direction>,
    start: &str,
) -> usize {
    let mut steps = 0;
    let mut current_location = start;
    let mut instruction_list = instructions.iter().cycle();
    loop {
        let (left, right) = map.get(current_location).unwrap();
        match instruction_list.next().unwrap() {
            Direction::Left => current_location = left,
            Direction::Right => current_location = right,
        };

        steps += 1;
        if current_location == "ZZZ" {
            break;
        }
    }
    steps
}

fn gcd(a: usize, b: usize) -> usize {
    if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}

fn lcm(a: usize, b: usize) -> usize {
    (a * b) / gcd(a, b)
}

pub fn part_1(input: &str) -> usize {
    let mut lines = input.lines();

    let instructions = parse_instructions(lines.next().unwrap());

    lines.next();

    let map = parse_choices(lines);
    find_path(&map, &instructions, "AAA")
}

pub fn part_2(input: &str) -> usize {
    let mut lines = input.lines();

    let instructions = parse_instructions(lines.next().unwrap());

    lines.next();

    let mut all_steps = vec![];
    let map = parse_choices(lines);
    for key in map.keys() {
        if !key.ends_with('A') {
            continue;
        }
        let mut steps = 0;
        let mut current_location = key;
        let mut instruction_list = instructions.iter().cycle();
        loop {
            let (left, right) = map.get(current_location).unwrap();
            match instruction_list.next().unwrap() {
                Direction::Left => current_location = left,
                Direction::Right => current_location = right,
            };

            steps += 1;
            if current_location.ends_with('Z') {
                all_steps.push(steps);
                break;
            }
        }
    }
    all_steps.into_iter().reduce(lcm).unwrap()
}

pub fn day_08() {
    let input = fs::read_to_string("inputs/day08/input-1.txt").expect("Failed to read file");

    let result_part1 = part_1(&input);
    println!("Result part 1: {result_part1}");
    assert_eq!(result_part1, 18827);

    let result_part2 = part_2(&input);
    println!("Result part 2: {result_part2}");
    assert_eq!(result_part2, 20220305520997);
}
