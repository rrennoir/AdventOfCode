use std::fs;

pub fn day_01() {
    let input = fs::read_to_string("day01/input.txt").expect("Couldn't read file");

    let numbers_str = [
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    ];

    let mut sum: u64 = 0;
    for line in input.lines() {
        dbg!(&line);

        let mut letters: Vec<u8> = vec![];
        let mut numbers: Vec<u8> = vec![];

        let chars = line.as_bytes();
        for char in chars {
            if char > &b'0' && char <= &b'9' {
                letters.clear();
                numbers.push(char - 48);
                continue;
            }
            letters.push(*char);
            let word = String::from_utf8(letters.clone()).unwrap();

            for digit_str in numbers_str {
                if !word.ends_with(digit_str) {
                    continue;
                }

                match numbers_str.iter().position(|&nbr_str| nbr_str == digit_str) {
                    Some(number) => {
                        numbers.push((number + 1) as u8);
                    }
                    None => panic!("WTF happened there"),
                }
                break;
            }
        }

        if numbers.is_empty() {
            continue;
        }

        let nb1 = (numbers.first().unwrap() * 10) as u64;
        let nb2 = (*numbers.last().unwrap()) as u64;

        let number = dbg!(nb1 + nb2);

        sum += number;

        numbers.clear();
    }

    dbg!(sum);
}
