pub mod common;
pub mod day01;
pub mod day02;
pub mod day03;
pub mod day04;
pub mod day05;
pub mod day06;
pub mod day07;
pub mod day08;
pub mod day09;

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn day05_part1() {
        let result = day05::part_1(include_str!("../inputs/day05/test-1.txt"));
        assert_eq!(result, 35)
    }

    #[test]
    fn day05_part2() {
        let result = day05::part_2(include_str!("../inputs/day05/test-1.txt"));
        assert_eq!(result, 46)
    }

    #[test]
    fn day06_part1() {
        let result = day06::part_1(include_str!("../inputs/day06/test-1.txt"));
        assert_eq!(result, 288)
    }

    #[test]
    fn day06_part2() {
        let result = day06::part_2(include_str!("../inputs/day06/test-1.txt"));
        assert_eq!(result, 71503)
    }

    #[test]
    fn day07_part1() {
        let result = day07::part_1(include_str!("../inputs/day07/test-1.txt"));
        assert_eq!(result, 6440)
    }

    #[test]
    fn day07_part2() {
        let result = day07::part_2(include_str!("../inputs/day07/test-2.txt"));
        assert_eq!(result, 5905)
    }

    #[test]
    fn day08_part1() {
        let result = day08::part_1(include_str!("../inputs/day08/test-1.txt"));
        assert_eq!(result, 6)
    }

    #[test]
    fn day08_part2() {
        let result = day08::part_2(include_str!("../inputs/day08/test-2.txt"));
        assert_eq!(result, 6)
    }

    #[test]
    fn day09_part1() {
        let result = day09::part_1(include_str!("../inputs/day09/test-1.txt"));
        assert_eq!(result, 114)
    }

    #[test]
    fn day09_part2() {
        let result = day09::part_2(include_str!("../inputs/day09/test-1.txt"));
        assert_eq!(result, 2)
    }
}
