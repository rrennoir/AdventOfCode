use std::{
    collections::{HashSet, VecDeque},
    fs,
};

struct Point {
    shape: char,
    x: usize,
    y: usize,
}

impl Point {
    fn new(shape: char, x: usize, y: usize) -> Point {
        Point { shape, x, y }
    }
}

fn parse_map(input: &str) -> Vec<Vec<char>> {
    input
        .lines()
        .map(|line| line.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>()
}

fn find_start_location(map: &Vec<Vec<char>>) -> (usize, usize) {
    for (y, pipe_line) in map.iter().enumerate() {
        for (x, pipe) in pipe_line.iter().enumerate() {
            if *pipe == 'S' {
                return (x, y);
            }
        }
    }

    panic!("Failed to find start location")
}

fn find_path_bfs(
    map: &Vec<Vec<char>>,
    visited_map: &mut [Vec<bool>],
    start: (usize, usize),
) -> (usize, char) {
    let max_y = map.len() - 1;
    let max_x = map[0].len() - 1;
    let mut path_queue: VecDeque<(usize, usize)> = VecDeque::new();
    path_queue.push_back(start);
    let mut visited_count = 1;
    let mut s_possibility = HashSet::from(['|', '-', 'J', 'L', '7', 'F']);
    while path_queue.len() > 0 {
        let location = path_queue.pop_front().unwrap();
        let pipe = map[location.1][location.0];

        if ['S', '|', 'L', 'J'].contains(&pipe)
            && location.1 > 0
            && !visited_map[location.1 - 1][location.0]
            && ['|', '7', 'F'].contains(&map[location.1 - 1][location.0])
        {
            visited_map[location.1 - 1][location.0] = true;
            path_queue.push_back((location.0, location.1 - 1));
            visited_count += 1;
            if pipe == 'S' {
                s_possibility = HashSet::from_iter(
                    s_possibility
                        .intersection(&HashSet::from(['|', 'L', 'J']))
                        .cloned(),
                );
            }
        }

        if ['S', '|', '7', 'F'].contains(&pipe)
            && location.1 < max_y
            && !visited_map[location.1 + 1][location.0]
            && ['|', 'L', 'J'].contains(&map[location.1 + 1][location.0])
        {
            visited_map[location.1 + 1][location.0] = true;
            path_queue.push_back((location.0, location.1 + 1));
            visited_count += 1;
            if pipe == 'S' {
                s_possibility = HashSet::from_iter(
                    s_possibility
                        .intersection(&HashSet::from(['|', '7', 'F']))
                        .cloned(),
                );
            }
        }
        if ['S', '-', 'F', 'L'].contains(&pipe)
            && location.0 < max_x
            && !visited_map[location.1][location.0 + 1]
            && ['-', '7', 'J'].contains(&map[location.1][location.0 + 1])
        {
            visited_map[location.1][location.0 + 1] = true;
            path_queue.push_back((location.0 + 1, location.1));
            visited_count += 1;

            if pipe == 'S' {
                s_possibility = HashSet::from_iter(
                    s_possibility
                        .intersection(&HashSet::from(['-', 'L', 'F']))
                        .cloned(),
                );
            }
        }
        if ['S', '-', '7', 'J'].contains(&pipe)
            && location.0 > 0
            && !visited_map[location.1][location.0 - 1]
            && ['-', 'L', 'F'].contains(&map[location.1][location.0 - 1])
        {
            visited_map[location.1][location.0 - 1] = true;
            path_queue.push_back((location.0 - 1, location.1));
            visited_count += 1;

            if pipe == 'S' {
                s_possibility = HashSet::from_iter(
                    s_possibility
                        .intersection(&HashSet::from(['-', '7', 'J']))
                        .cloned(),
                );
            }
        }
    }
    (
        visited_count / 2,
        s_possibility.iter().take(1).last().unwrap().to_owned(),
    )
}

fn get_outside_points(map: &Vec<Vec<char>>) -> HashSet<(usize, usize)> {
    let mut outside = HashSet::new();

    for (y, row) in map.iter().enumerate() {
        let mut inside = false;
        let mut up = None;
        for (x, pipe) in row.iter().enumerate() {
            match *pipe {
                '|' => {
                    inside = !inside;
                }
                '-' => {}
                'L' | 'F' => {
                    up = Some(*pipe == 'L');
                }
                '7' | 'J' => {
                    let condition = match up {
                        Some(tmp) => match tmp {
                            true => 'J',
                            false => '7',
                        },
                        None => panic!("up can't be None at this point"),
                    };

                    if *pipe != condition {
                        inside = !inside;
                    }

                    up = None;
                }
                '.' => {}
                _ => unreachable!(),
            }

            if !inside {
                outside.insert((x, y));
            }
        }
    }
    outside
}

pub fn part_1(input: &str) -> usize {
    let map = parse_map(input);
    let start = find_start_location(&map);

    let mut visited_map: Vec<Vec<bool>> = Vec::with_capacity(map.len());
    let column_count = map[0].len();
    for y in 0..map.len() {
        let mut column = Vec::with_capacity(column_count);
        for x in 0..column_count {
            if (x, y) == start {
                column.push(true);
                continue;
            }
            column.push(false);
        }
        visited_map.push(column);
    }
    let (distance, _) = find_path_bfs(&map, visited_map.as_mut_slice(), start);
    distance
}

pub fn part_2(input: &str) -> usize {
    let mut map = parse_map(input);
    let start = find_start_location(&map);

    let mut visited_map: Vec<Vec<bool>> = Vec::with_capacity(map.len());
    let column_count = map[0].len();
    for y in 0..map.len() {
        let mut column = Vec::with_capacity(column_count);
        for x in 0..column_count {
            if (x, y) == start {
                column.push(true);
                continue;
            }
            column.push(false);
        }
        visited_map.push(column);
    }

    let (_, s_char) = find_path_bfs(&map, visited_map.as_mut_slice(), start);

    let mut pipe_loop = HashSet::new();
    for (y, row) in map.iter_mut().enumerate() {
        for (x, ch) in row.iter_mut().enumerate() {
            if !visited_map[y][x] {
                *ch = '.';
            } else {
                pipe_loop.insert((x, y));
            }
        }
    }

    map[start.1][start.0] = s_char;
    let outside = get_outside_points(&map);
    map.len() * map[0].len() - outside.union(&pipe_loop).count()
}

pub fn day_10() {
    let input = fs::read_to_string("inputs/day10/input-1.txt").expect("Failed to read input");
    let result_part1 = part_1(&input);
    println!("Result of part 1: {result_part1}");
    assert_eq!(result_part1, 6903);

    let result_part2 = part_2(&input);
    println!("Result of part 2: {result_part2}");
    assert_eq!(result_part2, 265);
}

#[test]
fn test_part1() {
    let result = part_1(include_str!("../inputs/day10/test-1.txt"));
    assert_eq!(result, 8)
}

#[test]
fn test_part2() {
    let result = part_2(include_str!("../inputs/day10/test-2.txt"));
    assert_eq!(result, 8)
}
