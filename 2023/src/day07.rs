use std::{collections::HashMap, fs};

#[derive(Debug, PartialEq, Eq)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeKind,
    FullHouse,
    FourKind,
    FiveKind,
}

#[derive(Debug)]
struct Hand {
    hand_type: HandType,
    cards: Vec<u8>,
    bid: usize,
}

impl Hand {
    fn new(hand_type: HandType, cards: Vec<u8>, bid: usize) -> Hand {
        Hand {
            hand_type,
            cards,
            bid,
        }
    }
}

fn get_hand_type(mut pairs: HashMap<u8, usize>, joker_special: bool) -> HandType {
    let pair_count;
    if joker_special {
        let joker_count = pairs.get(&1).unwrap_or(&0).clone();
        pairs.remove(&1);
        pair_count = pairs.values().max().unwrap_or(&0) + joker_count;
    } else {
        pair_count = *pairs.values().max().unwrap();
    }

    match pair_count {
        5 => HandType::FiveKind,
        4 => HandType::FourKind,
        3 => {
            if pairs.values().find(|val| **val == 2).is_some() {
                HandType::FullHouse
            } else {
                HandType::ThreeKind
            }
        }
        2 => {
            if pairs.values().filter(|val| **val == 2).count() == 2 {
                HandType::TwoPair
            } else {
                HandType::OnePair
            }
        }
        1 => HandType::HighCard,
        _ => unreachable!(),
    }
}

fn compute_score(hands: Vec<Hand>) -> usize {
    let mut score = 0;
    let mut current_rank = 1;
    for hand_rank in 0..=6 {
        let hand_type = match hand_rank {
            6 => HandType::FiveKind,
            5 => HandType::FourKind,
            4 => HandType::FullHouse,
            3 => HandType::ThreeKind,
            2 => HandType::TwoPair,
            1 => HandType::OnePair,
            0 => HandType::HighCard,
            _ => unreachable!(),
        };

        let mut hs = hands
            .iter()
            .filter(|hand| hand.hand_type == hand_type)
            .collect::<Vec<&Hand>>();

        while hs.len() > 0 {
            let mut worst_id = 0;
            for (i, h) in hs.iter().enumerate() {
                let mut j = 0;
                while j < h.cards.len() {
                    match h.cards[j].cmp(&hs[worst_id].cards[j]) {
                        std::cmp::Ordering::Less => {
                            worst_id = i;
                        }
                        std::cmp::Ordering::Greater => break,
                        std::cmp::Ordering::Equal => (),
                    };
                    j += 1;
                }
            }
            score += current_rank * hs[worst_id].bid;
            hs.remove(worst_id);
            current_rank += 1;
        }
    }
    score
}

pub fn part_1(input: &str) -> usize {
    let mut hands: Vec<Hand> = vec![];
    for line in input.lines() {
        let (cards, bid) = line.split_once(" ").unwrap();

        let mut pairs: HashMap<u8, usize> = HashMap::new();
        let mut cards_strength: Vec<u8> = vec![];
        for card in cards.as_bytes() {
            let strength = match card {
                b'2'..=b'9' => card - b'0',
                b'T' => 10,
                b'J' => 11,
                b'Q' => 12,
                b'K' => 13,
                b'A' => 14,
                _ => unreachable!(),
            };
            cards_strength.push(strength);
            pairs
                .entry(strength)
                .and_modify(|val| *val += 1)
                .or_insert(1);
        }

        let hand_type = get_hand_type(pairs, false);
        let bid = bid.parse::<usize>().unwrap();
        hands.push(Hand::new(hand_type, cards_strength, bid));
    }

    compute_score(hands)
}

pub fn part_2(input: &str) -> usize {
    let mut hands: Vec<Hand> = vec![];
    for line in input.lines() {
        let (cards, bid) = line.split_once(" ").unwrap();

        let mut pairs: HashMap<u8, usize> = HashMap::new();
        let mut cards_strength: Vec<u8> = vec![];
        for card in cards.as_bytes() {
            let strength = match card {
                b'2'..=b'9' => card - b'0',
                b'T' => 10,
                b'J' => 1,
                b'Q' => 12,
                b'K' => 13,
                b'A' => 14,
                _ => unreachable!(),
            };
            cards_strength.push(strength);
            pairs
                .entry(strength)
                .and_modify(|val| *val += 1)
                .or_insert(1);
        }

        let hand_type = get_hand_type(pairs, true);
        let bid = bid.parse::<usize>().unwrap();
        hands.push(Hand::new(hand_type, cards_strength, bid));
    }

    let score = compute_score(hands);
    score
}

pub fn day_07() {
    let input = fs::read_to_string("inputs/day07/input-1.txt").unwrap();
    let result_part1 = part_1(&input);
    assert_eq!(result_part1, 250453939);
    println!("Part 1: {}", result_part1);

    todo!("Find bug, value too high");
    let result_part2 = part_2(&input);
    assert_eq!(result_part2, 248652697);
    println!("Part 2: {}", result_part2);
}
