use std::fs;

fn get_readings(input: &str) -> Vec<Vec<isize>> {
    input
        .lines()
        .map(|line| {
            line.split_whitespace()
                .map(|val| val.parse::<isize>().unwrap())
                .collect()
        })
        .collect::<Vec<Vec<isize>>>()
}

pub fn compute_steps(initial_reading: Vec<isize>) -> Vec<Vec<isize>> {
    let mut steps = vec![initial_reading.clone()];
    let mut initial = initial_reading;
    loop {
        let step = initial
            .windows(2)
            .map(|vals| vals[1] - vals[0])
            .collect::<Vec<isize>>();

        if step.iter().all(|&x| x == 0) {
            break;
        }
        initial = step.clone();
        steps.push(step.clone());
    }
    steps
}

pub fn part_1(input: &str) -> isize {
    let readings = get_readings(input);
    let mut history = vec![];
    for reading in readings {
        let mut steps = compute_steps(reading);
        let mut prediction: isize = 0;
        for i in (0..steps.len()).rev() {
            let last = steps[i].last().unwrap().clone();
            prediction = last + prediction;
            steps[i].push(prediction);
        }
        history.push(prediction);
    }

    history.iter().sum()
}

pub fn part_2(input: &str) -> isize {
    let readings = get_readings(input);
    let mut history = vec![];
    for reading in readings {
        let mut steps = compute_steps(reading);
        let mut prediction: isize = 0;
        for i in (0..steps.len()).rev() {
            let first = steps[i].first().unwrap().clone();
            prediction = first - prediction;
            steps[i].insert(0, prediction);
        }
        history.push(prediction);
    }

    history.iter().sum()
}

pub fn day_09() {
    let input = fs::read_to_string("inputs/day09/input-1.txt").expect("Failed to read input file");

    let result_part1 = part_1(&input);
    println!("Result part 1: {result_part1}");
    assert_eq!(result_part1, 1969958987);

    let result_part2 = part_2(&input);
    println!("Result part 2: {result_part2}");
    assert_eq!(result_part2, 1068);
}
