use std::env;

pub mod common;
pub mod day01;
pub mod day02;
pub mod day03;
pub mod day04;
pub mod day05;
pub mod day06;
pub mod day07;
pub mod day08;
pub mod day09;
pub mod day10;

fn main() {
    let args = env::args().into_iter().collect::<Vec<String>>();

    match args[1].as_str() {
        "01" | "1" => day01::day_01(),
        "02" | "2" => day02::day_02(),
        "03" | "3" => day03::day_03(),
        "04" | "4" => day04::day_04(),
        "05" | "5" => day05::day_05(),
        "06" | "6" => day06::day_06(),
        "07" | "7" => day07::day_07(),
        "08" | "8" => day08::day_08(),
        "09" | "9" => day09::day_09(),
        "10" => day10::day_10(),
        _ => unimplemented!(),
    };
}
