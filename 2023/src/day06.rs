use std::fs;

pub fn part_1(input: &str) -> usize {
    let mut lines = input.lines();

    let time_line = lines.next().unwrap();
    let times = time_line
        .split_once(":")
        .unwrap()
        .1
        .trim()
        .split(" ")
        .filter(|txt| *txt != "")
        .map(|value| value.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();

    let distance_line = lines.next().unwrap();
    let distances = distance_line
        .split_once(":")
        .unwrap()
        .1
        .trim()
        .split(" ")
        .filter(|txt| *txt != "")
        .map(|value| value.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();

    let mut i = 0;
    let mut win_possibility: Vec<usize> = vec![];
    while i < times.len() {
        let max_time = times[i] - 1;
        let mut win = 0;
        for pressing_time in 1..max_time {
            let distance = pressing_time * (times[i] - pressing_time);
            if distance > distances[i] {
                // println!("winning distance {distance} mm, pressing time {pressing_time}");
                win += 1;
            }
        }
        win_possibility.push(win);
        i += 1;
    }
    let mut total = 1;
    for wins in win_possibility {
        total *= wins;
    }

    total
}

pub fn part_2(input: &str) -> usize {
    let mut lines = input.lines();

    let time_line = lines.next().unwrap();
    let time = time_line
        .split_once(":")
        .unwrap()
        .1
        .trim()
        .split(" ")
        .filter(|txt| *txt != "")
        .collect::<Vec<&str>>()
        .join("")
        .parse::<usize>()
        .unwrap();

    let distance_line = lines.next().unwrap();
    let distance = distance_line
        .split_once(":")
        .unwrap()
        .1
        .trim()
        .split(" ")
        .filter(|txt| *txt != "")
        .collect::<Vec<&str>>()
        .join("")
        .parse::<usize>()
        .unwrap();

    let mut win_possibility: Vec<usize> = vec![];
    let max_time = time - 1;
    let mut wins = 0;
    for pressing_time in 1..max_time {
        let race_distance = pressing_time * (time - pressing_time);
        if race_distance > distance {
            // println!("winning distance {distance} mm, pressing time {pressing_time}");
            wins += 1;
        }
    }
    win_possibility.push(wins);
    let mut total = 1;
    for wins in win_possibility {
        total *= wins;
    }

    total
}

pub fn day_06() {
    let input1 = fs::read_to_string("inputs/day06/input-1.txt").expect("Couldn't read file");

    let result_part1 = part_1(&input1);
    println!("Result part 1: {result_part1}");
    assert_eq!(result_part1, 1159152);
    let result_part2 = part_2(&input1);
    println!("Result part 2: {result_part2}");
    assert_eq!(result_part2, 41513103);
}
