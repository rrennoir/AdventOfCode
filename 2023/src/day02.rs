use std::fs;

pub fn day_02() {
    let input = fs::read_to_string("day02/input-1.txt").expect("Couldn't read file");

    let mut sum = 0;
    let mut power = 0;

    let r_max = 12;
    let g_max = 13;
    let b_max = 14;

    let mut game_id = 1;
    for line in input.lines() {
        let mut c_iter = line.as_bytes().iter();

        loop {
            if let Some(b':') = c_iter.next() {
                break;
            }
        }

        let mut val: usize = 0;
        let mut r: usize = 0;
        let mut g: usize = 0;
        let mut b: usize = 0;
        for c in c_iter {
            match c {
                b'0'..=b'9' => val = val * 10 + (c - b'0') as usize,
                b'r' => {
                    r = r.max(val);
                    val = 0;
                }
                b'g' => {
                    g = g.max(val);
                    val = 0;
                }
                b'b' => {
                    b = b.max(val);
                    val = 0;
                }
                _ => (),
            }
        }

        if r <= r_max && g <= g_max && b <= b_max {
            sum += game_id;
        }

        power += r * g * b;

        game_id += 1;
    }
    dbg!(sum);
    dbg!(power);
}
