use std::{collections::HashMap, fs};

fn part_one(input: &str) -> usize {
    let lines = input.lines();

    let mut points: usize = 0;
    for line in lines {
        let cards = line.split_once(":").unwrap().1;

        let (winning, ours) = cards.split_once(" | ").unwrap();

        let winning_numbers = winning
            .split_ascii_whitespace()
            .map(|card| card.parse::<usize>().unwrap())
            .collect::<Vec<usize>>();

        let our_numbers = ours
            .split_ascii_whitespace()
            .map(|card| card.parse::<usize>().unwrap())
            .collect::<Vec<usize>>();

        let mut deck_points = 0;
        for number in our_numbers {
            if !winning_numbers.contains(&number) {
                continue;
            }

            if deck_points == 0 {
                deck_points = 1;
            } else {
                deck_points *= 2;
            }
        }
        points += deck_points;
    }

    points
}

fn part_two(input: &str) -> usize {
    let mut decks_ref_counter: HashMap<usize, usize> = HashMap::new();
    let mut card_id = 0;
    for line in input.lines() {
        let card_count = decks_ref_counter
            .entry(card_id)
            .and_modify(|ref_count| *ref_count += 1)
            .or_insert(1)
            .clone();

        let cards = line.split_once(":").unwrap().1;

        let (winning, ours) = cards.split_once(" | ").unwrap();

        let winning_numbers = winning
            .split_ascii_whitespace()
            .map(|card| card.parse::<usize>().unwrap())
            .collect::<Vec<usize>>();

        let our_numbers = ours
            .split_ascii_whitespace()
            .map(|card| card.parse::<usize>().unwrap())
            .collect::<Vec<usize>>();

        let mut score = 1;
        for number in &our_numbers {
            if !winning_numbers.contains(&number) {
                continue;
            }
            decks_ref_counter
                .entry(score + card_id)
                .and_modify(|ref_count| *ref_count += card_count)
                .or_insert(card_count);

            score += 1;
        }

        card_id += 1;
    }

    decks_ref_counter.values().sum::<usize>()
}

pub fn day_04() {
    let input1 = fs::read_to_string("day04/input-1.txt").expect("Couldn't read file");
    println!("Part 1: {}", part_one(&input1));

    let input2 = fs::read_to_string("day04/input-2.txt").expect("Couldn't read file");
    println!("Part 2: {}", part_two(&input2));
}
