use std::cmp;
use std::{collections::HashMap, fs};

struct MapRange {
    destination: usize,
    source: usize,
    length: usize,
}

struct Mapping {
    origin: String,
    destination: String,
    ranges: Vec<MapRange>,
}

impl Mapping {
    fn new(src: &str, dst: &str) -> Mapping {
        Mapping {
            origin: src.to_string(),
            destination: dst.to_string(),
            ranges: vec![],
        }
    }
}

pub fn part_1(input: &str) -> usize {
    let lines = input.lines().into_iter().collect::<Vec<&str>>();

    let seeds = lines[0]
        .split_once(": ")
        .unwrap()
        .1
        .split_ascii_whitespace()
        .map(|seed| seed.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();

    let mut maps: HashMap<String, Mapping> = HashMap::new();
    let mut current_src = "";
    for line in &lines[1..] {
        if line == &"" {
            continue;
        }

        match line.split_once(" ").unwrap().0.split_once("-to-") {
            Some(map) => {
                maps.insert(map.0.to_string(), Mapping::new(map.0, map.1));
                current_src = map.0;
                continue;
            }
            None => (),
        };

        let values = line
            .split_ascii_whitespace()
            .map(|val| val.parse::<usize>().unwrap())
            .collect::<Vec<usize>>();

        let range = MapRange {
            destination: values[0],
            source: values[1],
            length: values[2],
        };

        maps.entry(current_src.to_string())
            .and_modify(|mapping| mapping.ranges.push(range));
    }

    let mut best = usize::MAX;
    for seed in seeds {
        println!("Doing seed {seed}");
        let mut next_dst = "seed".to_string();

        let mut old_val = seed;
        let mut new_val = seed;
        loop {
            let mapping = maps.get(&next_dst).unwrap();
            for range in &mapping.ranges {
                if range.source <= new_val && new_val < range.source + range.length {
                    old_val = new_val;
                    new_val =
                        range.destination + range.length - (range.source + range.length - new_val);
                    break;
                }
            }

            println!(
                "Mapping {old_val} {} -> {} {new_val}",
                mapping.origin, mapping.destination
            );
            next_dst = mapping.destination.clone();
            if next_dst == "location" {
                break;
            }
        }
        best = cmp::min(new_val, best);
    }
    best
}

pub fn part_2(input: &str) -> usize {
    let lines = input.lines().into_iter().collect::<Vec<&str>>();

    let seeds = lines[0]
        .split_once(": ")
        .unwrap()
        .1
        .split_ascii_whitespace()
        .map(|seed| seed.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();

    let mut maps: HashMap<String, Mapping> = HashMap::new();
    let mut current_src = "";
    for line in &lines[1..] {
        // dbg!(&line);

        if line == &"" {
            continue;
        }

        match line.split_once(" ").unwrap().0.split_once("-to-") {
            Some(map) => {
                maps.insert(map.0.to_string(), Mapping::new(map.0, map.1));
                current_src = map.0;
                continue;
            }
            None => (),
        };

        let values = line
            .split_ascii_whitespace()
            .map(|val| val.parse::<usize>().unwrap())
            .collect::<Vec<usize>>();

        let range = MapRange {
            destination: values[0],
            source: values[1],
            length: values[2],
        };

        maps.entry(current_src.to_string())
            .and_modify(|mapping| mapping.ranges.push(range));
    }

    let mut i = 0;
    let mut best = usize::MAX;
    let mut new_ranges: Vec<(usize, usize)> = vec![];
    while i < seeds.len() {
        // println!("Seed start at {} with range {}", seeds[i], seeds[i + 1]);

        if new_ranges.is_empty() {
            new_ranges.push((seeds[i], seeds[i + 1]));
            continue;
        }

        let mut overlap = false;
        for new_range in new_ranges.iter_mut() {
            let a = new_range.0;
            let b = new_range.0 + new_range.1;
            let c = seeds[i];
            let d = seeds[i] + seeds[i + 1];

            // println!("Range {a}..{b} vs {c}..{d}");

            if a >= c && d <= b && d > a {
                new_range.0 = c;
                // println!("found new min {c}");
                overlap = true;
            }

            if b <= d && a <= c && c < b {
                new_range.1 = d - new_range.0;
                // println!("found new max {}", d - new_range.0);
                overlap = true;
            }
        }

        if !overlap {
            new_ranges.push((seeds[i], seeds[i + 1]))
        }

        let mut seed = seeds[i];
        while seed < seeds[i] + seeds[i + 1] {
            let mut next_dst = "seed".to_string();
            let mut new_val = seed;
            loop {
                let mapping = maps.get(&next_dst).unwrap();
                for range in &mapping.ranges {
                    if range.source <= new_val && new_val < range.source + range.length {
                        new_val = range.destination + range.length
                            - (range.source + range.length - new_val);
                        break;
                    }
                }

                next_dst = mapping.destination.clone();
                if next_dst == "location" {
                    break;
                }
            }
            best = cmp::min(new_val, best);
            seed += 1;
        }
        i += 2;
    }
    best
}

pub fn day_05() {
    let input = fs::read_to_string("inputs/day05/input-1.txt").expect("Couldn't read file");

    let result_part1 = part_1(&input);
    println!("Result part 1: {result_part1}");
    assert_eq!(result_part1, 51752125);

    let result_part2 = part_2(&input);
    println!("Result part 2: {result_part2}");
    assert_eq!(result_part2, 12634632);
}
